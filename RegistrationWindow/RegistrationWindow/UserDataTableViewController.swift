//
//  UserDataTableViewController.swift
//  RegistrationWindow
//
//  Created by Sergey on 3/10/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

import UIKit
import DatePickerDialog
import CountryPicker

let heightUnknownCell = 200.0
let heightUserInfoCell = 113.0
let heightRegistrationButtonCell = 145.0
let statusBarHeight: CGFloat = 20.0

class UserDataTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PickerContainerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    private var dateFormatter: DateFormatter = DateFormatter()
    private var cells: [CellModel] = [CellModel]()
    private var currentCountry = ""
    private var quantityCommonCell = 0
    public var userDataDict: NSMutableDictionary = [:]
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerContainer: PickerContainer!
    @IBOutlet weak var bottomConstOfTabView: NSLayoutConstraint!
    
    let imagePicker = UIImagePickerController()
    
    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = color
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let center = NotificationCenter.default
        center.addObserver(self,
                           selector: #selector(keyboardWillShow),
                           name: .UIKeyboardWillShow,
                           object: nil)
        
        center.addObserver(self,
                           selector: #selector(keyboardWillHide),
                           name: .UIKeyboardWillHide,
                           object: nil)
        
        self.title = "NavigationTitle".localized
        
        dateFormatter.dateFormat = "MMMM dd yyyy"
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage();

        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
       
        let navBarFrame = self.navigationController?.navigationBar.frame
        
        let frame = CGRect(x: 0, y: 0, width: navBarFrame!.width, height: navBarFrame!.height + statusBarHeight)
        blurEffectView.frame = frame
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        self.tableView.estimatedRowHeight = 143
        
        let userPhoto = CellModel(cellType: .Header, title: "AddPhotoLabel", placeholder: "", inputText: "")
        userPhoto.selector = "imagePickerPressed"
        cells.append(userPhoto)
        
        let firstName = CellModel(cellType: .Common, title: "FirstName", placeholder: "", inputText: "")
        cells.append(firstName)
        
        let lastName = CellModel(cellType: .Common, title: "LastName", placeholder: "", inputText: "")
        cells.append(lastName)
        
        let birthday = CellModel(cellType: .Common, title: "Birthday", placeholder: "DatePlaceholder", inputText: "")
        birthday.selector = "datePickerPressed"
        cells.append(birthday)
        
        let familyStatus = CellModel(cellType: .Common, title: "FamilyStatus", placeholder: "FamilyPlaceholder", inputText: "")
        cells.append(familyStatus)
        
        let country = CellModel(cellType: .Common, title: "UserCountry", placeholder: "CountryPlaceholder", inputText: "")
        country.selector = "countryPickerPressed"
        cells.append(country)
        
        let city = CellModel(cellType: .Common, title: "UserCity", placeholder: "CityPlaceholder", inputText: "")
        cells.append(city)
        
        let confirmCell = CellModel(cellType: .Confirmation, title: "", placeholder: "", inputText: "")
        confirmCell.selector = "registerButtonPressed"
        cells.append(confirmCell)
        
        pickerContainer.cancel()
        pickerContainer.delegate = self
        
        self.imagePicker.allowsEditing = false
        self.imagePicker.delegate = self
        
        for mod:CellModel in cells {
            if mod.type == .Common {
                quantityCommonCell += 1
            }
        }
    }

    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = cells[indexPath.row]
        
        switch model.type {
        case .None:
            let cell = tableView.dequeueReusableCell(withIdentifier: "", for: indexPath)
            return cell
            
        case .Header:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCellIdentifier", for: indexPath) as! HeaderCell
            cell.addPhotoLabel.text = model.titleText.localized
            if let castedSelectorName = model.selector {
                let sel = Selector(castedSelectorName)
                cell.imageButton.addTarget(self, action: sel, for: .touchUpInside)
                if let castedImage = model.image {
                    cell.userPhotoImage.image = castedImage
                    cell.userPhotoImage.contentMode = .scaleAspectFill
                    cell.userPhotoImage.layer.cornerRadius = cell.userPhotoImage.frame.width / 2
                }
            }
        
            return cell

        case .Common:
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputCellIdentifier", for: indexPath) as! InputCell
            
            cell.titleLabel.text = model.titleText.localized
            cell.textField.placeholder = model.inputPlaceholder.localized
            cell.textField.text = model.inputText
            if let castedSelectorName = model.selector {
                let sel = Selector(castedSelectorName)
                cell.pickerButton.addTarget(self, action: sel, for: .touchUpInside)
            } else {
                cell.pickerButton.isHidden = true
            }
            
            cell.textField.tag = indexPath.row - 1
            cell.textField.delegate = self
        
            return cell
            
        case .Confirmation:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmCellIdentifier", for: indexPath) as! ConfirmCell
            
            cell.registrationButton.setTitle("ButtonTitle".localized, for: .normal)
            
            if let castedSelectorName = model.selector {
                let sel = Selector(castedSelectorName)
                cell.registrationButton.addTarget(self, action: sel, for: .touchUpInside)
            } else {
                cell.registrationButton.isHidden = true
            }
            
            return cell
        }
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = cells[indexPath.row]
        
        switch model.type {
        case .None:
            return CGFloat(heightUnknownCell)
            
        case .Header:
            return UITableViewAutomaticDimension
            
        case .Common:
            return CGFloat(heightUserInfoCell)
            
        case .Confirmation:
            return CGFloat(heightRegistrationButtonCell)
        }
    }
    
    // MARK: - Pickers
    
    @objc func imagePickerPressed() {
        let alertController = UIAlertController(title: "Photo from:", message: "", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction.init(title: "Camera", style: .default) { action in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        alertController.addAction(cameraAction)
        
        let galleryAction = UIAlertAction.init(title: "Gallery", style: .default) { action in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        alertController.addAction(galleryAction)
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .destructive)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true)
    }
    
    @objc func datePickerPressed() {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.month = 1000
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        DatePickerDialog().show(title: "Select your date of birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
            if let dt = date {
                let model = self.cells[3]
                model.inputText = self.dateFormatter.string(from: dt)
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func countryPickerPressed() {
        
       pickerContainer.picker.alpha = 1

        let model = self.cells[5]
        currentCountry = model.inputText
        
        self.pickerContainer.presentPicker()
    }
    
    @objc func registerButtonPressed() {

        for modl:CellModel in cells {
            
            if (modl.image != nil) {
                userDataDict["Photo"] = String(format: "%@", modl.image!)
            } else if (modl.inputText != "") {
                userDataDict[String(format: "%@", modl.titleText)] = String(format: "%@", modl.inputText)
            }

            modl.inputText = ""
            modl.image = nil
        }
        
        print("\(userDataDict)")
        
        let alertController = UIAlertController(title: "Success!", message: "Your data successfully saved.", preferredStyle: .alert)
        self.present(alertController, animated: true)
        
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            
            self.tableView.reloadData()
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - PickerContainerDelegate
    
    public func cancelDidPressed() {
        let model = self.cells[5]
        model.inputText = currentCountry
        self.tableView.reloadData()
    }
    
    public func doneDidPressed(country: String?) {
        guard let castedCountry = country else {
            return
        }
        let model = self.cells[5]
        model.inputText = castedCountry
        self.tableView.reloadData()
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let model = self.cells[0]
            model.image = pickedImage
            self.tableView.reloadData()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: - UITextFieldDelegate

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == quantityCommonCell - 1 {
            textField.returnKeyType = .done
            
            let when = DispatchTime.now() + 0.05
            DispatchQueue.main.asyncAfter(deadline: when){
                
                let indexP = NSIndexPath(row: textField.tag, section: 0)
                self.tableView.scrollToRow(at: indexP as IndexPath, at: .top, animated: true)
            }
        } else {
            
            let indexP = NSIndexPath(row: textField.tag, section: 0)
            self.tableView.scrollToRow(at: indexP as IndexPath, at: .top, animated: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag != quantityCommonCell - 1 {

            let nextTxtFld = self.view.viewWithTag(textField.tag + 1) as! UITextField
            nextTxtFld.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let model = self.cells[textField.tag + 1]
        model.inputText = textField.text!
    }

     // MARK: - NSNotification
    
    public func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect {
            let contentInsets = keyboardSize.height
            bottomConstOfTabView.constant = contentInsets / 2
        }
    }
    
    public func keyboardWillHide(notification: NSNotification) {
            
            bottomConstOfTabView.constant = 0
    }
    
}
