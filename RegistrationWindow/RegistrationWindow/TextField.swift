//
//  TextField+InputView.swift
//  RegistrationWindow
//
//  Created by Sergey on 3/11/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

import Foundation
import UIKit

class TextField : UITextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }
    
    func setup() {
        let bounds = UIScreen.main.bounds;
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: bounds.size.width, height: 0))
        toolBar.barStyle = .default
        let doneButton = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(doneKeyboard))
        doneButton.setTitleTextAttributes([NSFontAttributeName : UIFont.systemFont(ofSize: 15), NSForegroundColorAttributeName : UIColor.black], for: .normal)
        toolBar.items = [UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), doneButton]
        toolBar.sizeToFit()
        self.inputAccessoryView = toolBar
    }
        
    func doneKeyboard() {
        self.resignFirstResponder()
    }
}
