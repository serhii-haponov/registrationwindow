//
//  InputCell.swift
//  RegistrationWindow
//
//  Created by Sergey on 3/10/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

import Foundation
import UIKit

class InputCell : UITableViewCell {
    @IBOutlet var pickerButton: UIButton!
    @IBOutlet var topSeparator: UIImageView!
    @IBOutlet var bottomSeparator: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var image = UIImage(named: "separator")!
        image = image.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .tile)
        topSeparator.image = image
        bottomSeparator.image = image
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        pickerButton.removeTarget(nil, action: nil, for: .allEvents)
        pickerButton.isHidden = false
    }
}

