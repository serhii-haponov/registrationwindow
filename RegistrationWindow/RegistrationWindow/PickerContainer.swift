//
//  PickerContainer.swift
//  RegistrationWindow
//
//  Created by Sergey on 3/11/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

import UIKit
import CountryPicker

@objc public protocol PickerContainerDelegate {
    func cancelDidPressed()
    func doneDidPressed(country: String?)
}

class PickerContainer : UIView, CountryPickerDelegate {
    open weak var delegate: PickerContainerDelegate?
    var currentCountry: String?
    
    @IBOutlet weak var picker: CountryPicker!
    
    @IBAction func done() {
        self.alpha = 0
        
        if let castedDelegate = delegate {
            castedDelegate.doneDidPressed(country: currentCountry)
        }
    }
    
    @IBAction func cancel() {
        self.alpha = 0
    
        if let castedDelegate = delegate {
            castedDelegate.cancelDidPressed()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
    }
    
    func presentPicker() {
        
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        picker.setCountry(code!)
        
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = false
        
          UIView .animate(withDuration: 0.4) {
           self.alpha = 1
        }
    }
    
    // MARK: - CountryPhoneCodePicker Delegate
    
    public func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        currentCountry = name
    }
}
