//
//  CellModel.swift
//  RegistrationWindow
//
//  Created by Sergey on 3/10/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

import Foundation
import UIKit

enum CellType {
    case None
    case Header
    case Common
    case Confirmation
}

class CellModel {
    public private(set) var type: CellType = .None
    public private(set) var titleText: String = ""
    public var selector: String?
    public var inputPlaceholder: String = ""
    public var inputText: String = ""
    public var image: UIImage?
    
    init(cellType: CellType, title: String, placeholder: String, inputText: String) {
        self.type = cellType
        self.titleText = title
        self.inputPlaceholder = placeholder
        self.inputText = inputText
    }
}
