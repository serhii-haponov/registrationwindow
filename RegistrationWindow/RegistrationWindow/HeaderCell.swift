//
//  HeaderCell.swift
//  RegistrationWindow
//
//  Created by Sergey on 3/10/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

import Foundation
import UIKit

class HeaderCell : UITableViewCell {
    @IBOutlet var imageButton: UIButton!
    @IBOutlet var addPhotoLabel: UILabel!
    @IBOutlet weak var userPhotoImage: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()
        
        userPhotoImage.image = nil
    }
}

