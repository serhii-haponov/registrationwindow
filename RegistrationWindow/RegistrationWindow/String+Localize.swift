//
//  String+Localize.swift
//  RegistrationWindow
//
//  Created by Sergey on 3/11/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        let langArray = UserDefaults.standard.object(forKey: "AppleLanguages") as! Array<String>
        let landIdentifier = langArray[0]
        let langId = Locale(identifier: landIdentifier)
        let path = Bundle.main.path(forResource: langId.languageCode, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: "Localize", bundle: bundle!, value: "", comment: "")
    }
}
