//
//  ConfirmCell.swift
//  RegistrationWindow
//
//  Created by Sergey on 3/10/17.
//  Copyright © 2017 Gaponov. All rights reserved.
//

import Foundation
import UIKit

class ConfirmCell : UITableViewCell {
    
    @IBOutlet var registrationButton: UIButton!
}
